import argparse
import collections
import json
import multiprocessing
import re
import subprocess
import sys
from multiprocessing.pool import ThreadPool
from pathlib import Path
import shutil

DisplayProgressBar = True
try:
    from tqdm import tqdm
except:
    DisplayProgressBar = False
    print("Tqdm was not found, we won't display a progress bar.\n"
          "If you'd like to use a progress bar install tqdm."
          "`pip install tqdm`", file=sys.stderr)


class Clazy(object):
    DefaultChecks = "level1"
    CategoryPattern = re.compile(r"\[-W(.*?)\]")
    EndPattern = re.compile(
        r"warning:[\S \n]*?\^", re.MULTILINE) if sys.platform == "win32" else re.compile(r"^(/)", re.MULTILINE)

    __ParseErrorKey = "_parser-error"

    def __init__(self, sourceDir, buildDir, jobs, checks):
        self.progressBar = None
        self.sourceDir = Path(sourceDir).resolve()
        self.buildDir = Path(buildDir).resolve()
        self.numberOfJobs = int(jobs)
        self.fixitDir = Path(__file__).parent / "fixit"
        self.checks = checks
        self.clazy_standalone_bin = shutil.which("clazy-standalone")

    def __sources(self) -> list[str]:
        result = subprocess.run(["git", "ls-files", "*.cpp"],
                                cwd=self.sourceDir, capture_output=True, encoding="UTF-8")
        if result.returncode != 0:
            return []
        return [self.sourceDir / s for s in result.stdout.strip().split("\n")]

    def __fixitName(self, src) -> Path:
        return self.fixitDir / re.sub(r"/|\\", "_", f"{src.relative_to(self.sourceDir)}.fix.yaml")

    def __run_clazy(self, source) -> list[tuple[str, str]]:
        out = []
        fixitName = self.__fixitName(source)
        fixitName.parent.mkdir(exist_ok=True, parents=True)
        # maybe one day clang could support diagnostics-format=json like gcc
        # "--extra-arg=-fdiagnostics-format=json"
        # https://www.rowleydownload.co.uk/arm/documentation/gnu/gcc/Diagnostic-Message-Formatting-Options.html
        result = subprocess.run([self.clazy_standalone_bin,
                                f"-checks={self.checks}",
                                f"-p={self.buildDir}/compile_commands.json",
                                f"-export-fixes={fixitName}",
                                source], cwd=self.buildDir, capture_output=True, encoding="UTF-8")
        log = []
        if self.progressBar:
            self.progressBar.update()
        else:
            log += [" ".join([str(x) for x in result.args])]
        if result.returncode != 0:
            log += [f"Failed to analyze: {source}"]
            if result.stdout:
                log += [f"stdout: {result.stdout}"]
            if result.stderr:
                log += [f"stderr: {result.stderr}"]
        else:
            text = result.stderr.strip()
            if text:
                # raw output for debugging
                #out.append((str(source), text))
                pos = 0
                warnings = []
                for match in Clazy.EndPattern.finditer(text):
                    end = match.span()[1]
                    warnings.append(text[pos:end])
                    pos = end

                # print(text[pos:])

                for w in warnings:
                    category = Clazy.CategoryPattern.findall(w)
                    if not category:
                        out.append(
                            (Clazy.__ParseErrorKey, [str(source), w, text]))
                    else:
                        out.append((category[-1], w))
        if log:
            print("\n".join(log), file=sys.stderr)
        return out

    def run(self) -> bool:
        if self.fixitDir.exists():
            shutil.rmtree(self.fixitDir)
        self.fixitDir.mkdir()

        sources = self.__sources()
        if DisplayProgressBar:
            self.progressBar = tqdm(total=len(sources))
        warnings = collections.defaultdict(list)
        result = []

        def run(sources):
            return self.__run_clazy(sources)

        # TODO: kill the jobs on ctrl+c
        with ThreadPool(self.numberOfJobs) as pool:
            r = pool.map_async(run, sources)
            pool.close()
            pool.join()
        result = r.get()

        for r in result:
            for c, v in r:
                if v is not None:
                    warnings[c].append(v)

        if Clazy.__ParseErrorKey in warnings:
            print("Some output could not properly be parsed", file=sys.stderr)
        outFile = Path(__file__).parent / f"clazy-report.json"
        with open(outFile, "wt") as out:
            json.dump(warnings, out, sort_keys=True, indent=2)
        if self.progressBar:
            self.progressBar.close()
        print(f"Report written to: {outFile}")
        return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Run clazy-standalone on a git repo and generate a report.')
    parser.add_argument("-s", "--src", dest="sourceDir", action="store",
                        help="The path to the src dir", required=True)
    parser.add_argument("-b", "--build", dest="buildDir", action="store",
                        help="The path to the build dir", required=True)
    parser.add_argument("-j", "--jobs", action="store",
                        help="The number of jobs to run in paralell, default: #cpu", default=multiprocessing.cpu_count())

    parser.add_argument("--checks", action="store",
                        help=f"The clazy check to run, default: {Clazy.DefaultChecks}", default=Clazy.DefaultChecks)

    args = parser.parse_args()
    c = Clazy(args.sourceDir, args.buildDir, args.jobs, args.checks)
    exit(0 if c.run() else 1)
